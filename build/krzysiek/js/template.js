"use strict";

// file: event.js

// end file: event.js

// file: global.js
function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}
// end file: global.js

// file: Modules/question.js
$(document).ready(function () {
    questionAccordion();
    exit();
    arrow();
});

function questionAccordion() {
    if ($("html.hs-inline-edit").length == 0) {
        $("section.question").each(function (i, e) {
            $(e).find(".panel-group").append($(e).find(".faq-box"));
            $(e).find(".hs_cos_wrapper_type_widget_container,.hs_cos_wrapper_type_custom_widget").remove();
        });
    }
    $("section.question .panel-group").attr("id", "accordion");
    $("section.question .panel-group .panel").each(function (i, e) {
        var href = $(e).find("h4.panel-title > a").attr("href");
        $(e).find("h4.panel-title > a").attr("href", href + (i + 1));

        var id = $(e).find(".panel-collapse").attr("id");
        $(e).find(".panel-collapse").attr("id", id + (i + 1));
    });
}

function exit() {
    $(".close").click(function () {
        $(this).parents('.panel-collapse').collapse('hide');
    });
}

function arrow() {
    $("h4.panel-title").children("a.collapsed").click(function () {
        if ($(this).parents("h4.panel-title").hasClass('addarrow')) {
            $(this).parents("h4.panel-title").removeClass('addarrow');
        } else {
            $("h4.panel-title").removeClass('addarrow');
            $(this).parents("h4.panel-title").addClass('addarrow');
        }
    });
}

// end file: Modules/question.js
//# sourceMappingURL=template.js.map
